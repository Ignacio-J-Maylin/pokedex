# Pokedex

Pokedex in a web app in angular 12 (node.js) that consumes an api made in spring boot with java 11, which consumes data from the api https://pokeapi.co/ (v2) but orders and filters them for the angle consumption.

## Getting started


clone de repository and run  : docker-compose up -d 
Local:
getPokemons : http://localhost:8083/api/pokemon?offset=1
getPokemonDetails : http://localhost:8083/api/pokemon/1
Swagger:  http://localhost:8083/swagger-ui.html#/api-controller
 


aws:
getPokemons : http://ec2-54-232-221-225.sa-east-1.compute.amazonaws.com:8083/api/pokemon?offset=1
getPokemonDetails :http://ec2-54-232-221-225.sa-east-1.compute.amazonaws.com:8083/api/pokemon/1
Swagger: http://ec2-54-232-221-225.sa-east-1.compute.amazonaws.com:8083/swagger-ui.html#/api-controller

