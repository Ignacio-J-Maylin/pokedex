package com.pokedex.model.dao;

public class TypeDAO {
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TypeDAO(String name) {
		super();
		this.name = name;
	}

	public TypeDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
