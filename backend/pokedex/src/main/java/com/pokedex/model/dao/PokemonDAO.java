package com.pokedex.model.dao;

import java.util.List;

public class PokemonDAO {

	private Long id;
	private String name;
	private SpritesDAO sprites;
	private List<TypesDAO> types;
	private Double weight;
	private List<AbilitiesDAO> abilities;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public SpritesDAO getSprites() {
		return sprites;
	}
	public void setSprites(SpritesDAO sprites) {
		this.sprites = sprites;
	}
	public List<TypesDAO> getTypes() {
		return types;
	}
	public void setTypes(List<TypesDAO> types) {
		this.types = types;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public List<AbilitiesDAO> getAbilities() {
		return abilities;
	}
	public void setAbilities(List<AbilitiesDAO> abilities) {
		this.abilities = abilities;
	}
	public PokemonDAO(Long id,String name, SpritesDAO sprites, List<TypesDAO> types, Double weight,
			List<AbilitiesDAO> abilities) {
		super();
		this.id = id;
		this.name = name;
		this.sprites = sprites;
		this.types = types;
		this.weight = weight;
		this.abilities = abilities;
	}
	public PokemonDAO() {
		super();
		// TODO Auto-generated constructor stub
	}


}
