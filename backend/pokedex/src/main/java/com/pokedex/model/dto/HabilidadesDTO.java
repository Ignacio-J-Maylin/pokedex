package com.pokedex.model.dto;

public class HabilidadesDTO {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HabilidadesDTO(String name) {
		super();
		this.name = name;
	}

	public HabilidadesDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
