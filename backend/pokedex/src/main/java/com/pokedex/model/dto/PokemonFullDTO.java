package com.pokedex.model.dto;

import java.util.List;

public class PokemonFullDTO {
	private Long id;
	private String name;
	private String foto;
	private String tipo;
	private String peso;
	private List<HabilidadesDTO> habilidades;
	private List<MovimientosDTO> movimientos;
	



	public PokemonFullDTO(Long id, String name, String foto, String tipo, String peso, List<HabilidadesDTO> habilidades,
			List<MovimientosDTO> movimientos) {
		super();
		this.id = id;
		this.name = name;
		this.foto = foto;
		this.tipo = tipo;
		this.peso = peso;
		this.habilidades = habilidades;
		this.movimientos = movimientos;
	}


	public PokemonFullDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getFoto() {
		return foto;
	}


	public void setFoto(String foto) {
		this.foto = foto;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getPeso() {
		return peso;
	}


	public void setPeso(String peso) {
		this.peso = peso;
	}


	public List<HabilidadesDTO> getHabilidades() {
		return habilidades;
	}


	public void setHabilidades(List<HabilidadesDTO> habilidades) {
		this.habilidades = habilidades;
	}


	public List<MovimientosDTO> getMovimientos() {
		return movimientos;
	}


	public void setMovimientos(List<MovimientosDTO> movimientos) {
		this.movimientos = movimientos;
	}


}
