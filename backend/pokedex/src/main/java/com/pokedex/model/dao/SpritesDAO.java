package com.pokedex.model.dao;

public class SpritesDAO {

	private String front_default;

	public String getFront_default() {
		return front_default;
	}

	public void setFront_default(String front_default) {
		this.front_default = front_default;
	}

	public SpritesDAO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SpritesDAO(String front_default) {
		super();
		this.front_default = front_default;
	}
	
	
}
