package com.pokedex.model.dao;

public class MovesDAO {
	
	private MoveDAO move;

	public MovesDAO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MovesDAO(MoveDAO move) {
		super();
		this.move = move;
	}

	public MoveDAO getMove() {
		return move;
	}

	public void setMove(MoveDAO move) {
		this.move = move;
	}


}
