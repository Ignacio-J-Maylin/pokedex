package com.pokedex.model.dto;

public class MovimientosDTO {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MovimientosDTO(String name) {
		super();
		this.name = name;
	}

	public MovimientosDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
