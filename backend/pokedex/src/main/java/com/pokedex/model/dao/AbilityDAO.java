package com.pokedex.model.dao;

public class AbilityDAO {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AbilityDAO(String name) {
		super();
		this.name = name;
	}

	public AbilityDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
