package com.pokedex.controller.service;

import com.pokedex.model.dao.PokemonDAO;
import com.pokedex.model.dao.PokemonFullDAO;
import com.pokedex.model.dto.PokemonDTO;
import com.pokedex.model.dto.PokemonFullDTO;

public interface MapperService {

	PokemonDTO pokemonDaoToPokemonDto(PokemonDAO pokemonDao);

	PokemonFullDTO pokemonDaoToPokemonFullDto(PokemonFullDAO pokemonDao);

}
