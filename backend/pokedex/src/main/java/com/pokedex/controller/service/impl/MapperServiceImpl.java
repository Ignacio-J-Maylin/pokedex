package com.pokedex.controller.service.impl;

import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.pokedex.controller.service.MapperService;
import com.pokedex.model.dao.PokemonDAO;
import com.pokedex.model.dao.PokemonFullDAO;
import com.pokedex.model.dto.HabilidadesDTO;
import com.pokedex.model.dto.MovimientosDTO;
import com.pokedex.model.dto.PokemonDTO;
import com.pokedex.model.dto.PokemonFullDTO;


@Service
public class MapperServiceImpl implements MapperService{

	private StringBuffer stringBuffer = new StringBuffer();
	

	@Override
	public PokemonDTO pokemonDaoToPokemonDto(PokemonDAO pokemonDAO) {
		PokemonDTO pokemonDTO = new PokemonDTO();
		this.resetearStringBuffer();
		pokemonDAO.getTypes().forEach(x->this.stringBuffer.append(x.getType().getName()+", "));
		pokemonDTO.setId(pokemonDAO.getId());
		pokemonDTO.setName(pokemonDAO.getName());
		pokemonDTO.setFoto(pokemonDAO.getSprites().getFront_default());
		pokemonDTO.setTipo(this.stringBuffer.substring(0, this.stringBuffer.length()-2).toString());
		pokemonDTO.setPeso(pokemonDAO.getWeight().toString());
		pokemonDTO.setHabilidades( pokemonDAO.getAbilities().stream()
		        .map(dao -> new HabilidadesDTO(dao.getAbility().getName()))
		        .collect(Collectors.toList()));
		return pokemonDTO;
	}
	
	
	private void resetearStringBuffer() {

		stringBuffer.delete(0, stringBuffer.length());
	}


	@Override
	public PokemonFullDTO pokemonDaoToPokemonFullDto(PokemonFullDAO pokemonDAO) {
		PokemonFullDTO pokemonDTO = new PokemonFullDTO();
		this.resetearStringBuffer();
		pokemonDAO.getTypes().forEach(x->this.stringBuffer.append(x.getType().getName()+", "));
		pokemonDTO.setId(pokemonDAO.getId());
		pokemonDTO.setName(pokemonDAO.getName());
		pokemonDTO.setFoto(pokemonDAO.getSprites().getFront_default());
		pokemonDTO.setTipo(this.stringBuffer.substring(0, this.stringBuffer.length()-2).toString());
		pokemonDTO.setPeso(pokemonDAO.getWeight().toString());
		pokemonDTO.setHabilidades( pokemonDAO.getAbilities().stream()
		        .map(dao -> new HabilidadesDTO(dao.getAbility().getName()))
		        .collect(Collectors.toList()));
		pokemonDTO.setMovimientos( pokemonDAO.getMoves().stream()
		        .map(dao -> new MovimientosDTO(dao.getMove().getName()))
		        .collect(Collectors.toList()));
		return pokemonDTO;
	}
	
}
