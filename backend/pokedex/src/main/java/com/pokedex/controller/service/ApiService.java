package com.pokedex.controller.service;

import java.util.concurrent.CompletableFuture;

import com.pokedex.model.dto.PokemonDTO;
import com.pokedex.model.dto.PokemonFullDTO;

public interface ApiService {

	PokemonDTO pokemonDTOfindById(Integer pokemonId);

	CompletableFuture<PokemonDTO> getPokemonAsync(int id);

	PokemonFullDTO pokemonDetalleDTOfindById(Integer pokemonId);

}
