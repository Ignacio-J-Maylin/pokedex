package com.pokedex.controller.service.impl;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.pokedex.controller.service.ApiService;
import com.pokedex.controller.service.MapperService;
import com.pokedex.model.dao.PokemonDAO;
import com.pokedex.model.dao.PokemonFullDAO;
import com.pokedex.model.dto.PokemonDTO;
import com.pokedex.model.dto.PokemonFullDTO;

@Service
public class ApiServiceImpl implements ApiService {


	private final String pokeapiBaseURL="https://pokeapi.co/api/v2/";
	private final RestTemplate restTemplate = new RestTemplate();
	@Autowired
	private MapperService mapperService;
	
	
	@Override
	public PokemonDTO pokemonDTOfindById(Integer pokemonId) {
		   String uri = pokeapiBaseURL+"pokemon/"+pokemonId;
		   PokemonDAO pokemonDao = restTemplate.getForObject(uri, PokemonDAO.class);
		   PokemonDTO pokemonDto = mapperService.pokemonDaoToPokemonDto(pokemonDao);
			return pokemonDto;
		}



	@Override
	@Async
	public CompletableFuture<PokemonDTO> getPokemonAsync(int id) {
		PokemonDTO pokemonDTO =pokemonDTOfindById(id);
		return CompletableFuture.completedFuture(pokemonDTO);
	}



	@Override
	public PokemonFullDTO pokemonDetalleDTOfindById(Integer pokemonId) {
		   String uri = pokeapiBaseURL+"pokemon/"+pokemonId;
		   PokemonFullDAO pokemonDao = restTemplate.getForObject(uri, PokemonFullDAO.class);
		   PokemonFullDTO pokemonDto = mapperService.pokemonDaoToPokemonFullDto(pokemonDao);
			return pokemonDto;
		}



	
}
