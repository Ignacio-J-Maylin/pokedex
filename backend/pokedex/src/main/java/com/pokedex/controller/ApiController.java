package com.pokedex.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pokedex.controller.service.ApiService;
import com.pokedex.model.dto.PokemonDTO;
import com.pokedex.model.dto.PokemonFullDTO;




@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api")
public class ApiController {
	
	@Autowired
	private ApiService apiService;

	@GetMapping("/pokemon/{pokemonId}")
	public  ResponseEntity<PokemonFullDTO>  getPokemon(@PathVariable(value = "pokemonId") Integer pokemonId)  {
		PokemonFullDTO result = apiService.pokemonDetalleDTOfindById(pokemonId);
		return ResponseEntity.ok().body(result);
	}
	@GetMapping("/pokemon")
	public  ResponseEntity<List<PokemonDTO>>  get10Pokemons(@RequestParam(value = "offset") Integer offset) throws Exception  {
		if (offset== null)
			offset = 1;
		List<PokemonDTO> result = get10PokemonsUsingThreads(offset);
		return ResponseEntity.ok().body(result);
	}
	
	
	private List<PokemonDTO> get10PokemonsUsingThreads(Integer offset)
			throws Exception {
		final int DIVICIONES = 10;
		List<CompletableFuture<PokemonDTO>> dtosHilos = new ArrayList<CompletableFuture<PokemonDTO>>();
		
			for (int i = 0; i < (DIVICIONES); i++) {
				dtosHilos.add(apiService.getPokemonAsync(offset+i));
			}
		CompletableFuture.allOf(dtosHilos.toArray(new CompletableFuture[dtosHilos.size()])).join();
		List<PokemonDTO> pokemons = new ArrayList<PokemonDTO>();
		for (CompletableFuture<PokemonDTO> dtoHilo : dtosHilos) {
			pokemons.add(dtoHilo.get());
		}
		Stream<PokemonDTO> streamdetalles = pokemons.stream()
				.sorted(Comparator.comparing(PokemonDTO::getId));
		return streamdetalles.collect(Collectors.toList());
	}
}